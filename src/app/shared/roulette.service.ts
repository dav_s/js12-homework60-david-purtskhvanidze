import { EventEmitter } from '@angular/core';
import { RouletteNumber } from './number.model';

export class RouletteService {

  newNumber = new EventEmitter<RouletteNumber[]>();

  private rouletteNumbers: RouletteNumber[] = [];
  private interval = 0;


  generateNumber() {
    const num = Math.floor(Math.random() * 37);
    this.rouletteNumbers.push(new RouletteNumber(num));
    this.newNumber.emit(this.rouletteNumbers);
  }

  start() {
    this.interval = setInterval(() => {
      this.generateNumber();
    }, 1000);
  }

  stop() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  getColor(num: number) {
    // В пределах от 1 до 10 и от 19 до 28 - нечетные числа красные, четные - черные
    // В пределах от 11 до 18 и от 29 до 36 - нечетные числа - черные, четные - красные
    // Для числа 0 возвращается zero
    let color = '';

    if (num >= 1 && num <= 10 || num >= 19 && num <= 28) {
      if (num % 2) {
        color = 'red';
      } else {
        color = 'black';
      }
    } else if (num >= 11 && num <= 18 || num >= 29 && num <= 36) {
      if (num % 2) {
        color = 'black';
      } else {
        color = 'red';
      }
    } else if (num === 0) {
      color = 'green';
    } else {
      color = 'unknown';
    }

    return color;
  }

}
