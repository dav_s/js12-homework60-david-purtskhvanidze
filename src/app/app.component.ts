import { Component, ElementRef, ViewChild } from '@angular/core';
import { RouletteService } from './shared/roulette.service';
import { RouletteNumber } from './shared/number.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild('inputBet') inputBet!: ElementRef;
  @ViewChild('inputRadioRed') inputRadioRed!: ElementRef;
  @ViewChild('inputRadioBlack') inputRadioBlack!: ElementRef;
  @ViewChild('inputRadioZero') inputRadioZero!: ElementRef;

  numbers: RouletteNumber[] = [];
  number!: number;
  red = '';
  black = '';
  zero = '';
  balance = 100;

  constructor(public rouletteService: RouletteService) {}

  ngOnInit(): void {
    this.rouletteService.newNumber.subscribe((rouletteNumber: RouletteNumber[]) => {
      this.numbers = rouletteNumber;
    });
  }

  reset() {
    this.numbers = [];
  }

  startGame() {
    this.rouletteService.start();
  }

  stopGame() {
    this.rouletteService.stop();
  }
}
