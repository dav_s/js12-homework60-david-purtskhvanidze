import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { RouletteService } from './shared/roulette.service';

@Directive({
  selector: '[appColor]'
})

export class ColorDirective {
  @Input() set appColor(num: number) {
    const color = this.rouletteService.getColor(num);
    this.renderer.addClass(this.el.nativeElement, color);
  }

  constructor(private el: ElementRef, private renderer: Renderer2, public rouletteService: RouletteService ) {}
}
